var SeleccionFutbol = function(datos){
	this.id = 0;
	this.Nombre = "";
	this.Apellidos = "";
	this.Edad = 0;

	this.Concentrarse = function(){
		console.log(this.Apellidos + " está concentrado.");
	};

	this.Viajar = function(){
		console.log(this.Apellidos + " está viajando.");
	};

	this.seleccion = function(datos){
		this.id = datos.id;
		this.Nombre = datos.nombre;
		this.Apellidos = datos.apellidos;
		this.Edad = datos.edad;
	};

	this.seleccion(datos);
};

var Entrenador = function(datos){
	this.idFederacion = "";

	this.dirigirPartido = function(){
		console.log("El entrenador " + this.Nombre + " " + this.Apellidos + " está dirigiendo un partido.");
	};

	this.dirigirEntrenamiento = function(){
		console.log("El entrenador " + this.Apellidos + "está dirigiendo un entrenamiento.");
	};

	this.entrenador = function(datos){
		SeleccionFutbol.call(this, datos);
		this.idFederacion = datos.idFederacion;
	};

	this.entrenador(datos);
};

var Masajista = function(datos){
	this.titulacion = "";
	this.aniosExperiencia = 0;

	this.darMasaje = function(){
		console.log(this.Apellidos + " está dando un masaje.");
	};

	this.masajista = function(datos){
		SeleccionFutbol.call(this, datos);
		this.titulacion = datos.titulacion;
		this.aniosExperiencia = datos.exp;
	};

	this.masajista(datos);
};

var Futbolista = function(datos){
	this.dorsal = 0;
	this.demarcacion = "";

	this.jugarPartido = function(){
		console.log(this.Apellidos + " está jugando un partido.");
	};

	this.entrenar = function(){
		console.log(this.Apellidos + " está entrenando.");
	};

	this.futbolista = function(datos){
		SeleccionFutbol.call(this, datos);
		this.dorsal = datos.dorsal;
		this.demarcacion = datos.demarcacion;
	};

	this.futbolista(datos);
};

var villamaravilla = new Futbolista({
	id: 0,
	dorsal: 9,
	demarcacion: "Delantero",
	nombre: "David",
	apellidos: "Villa",
	edad: 26
});

var pepe = new Masajista({
	id: 1,
	titulacion: "Curso superior fisioterapia",
	exp: 12,
	nombre: "Pepe",
	apellidos: "Basurilla",
	edad: 50
});

var lenrique = new Entrenador({
	id: 2,
	idFederacion: 0,
	nombre: "Luis",
	apellidos: "Enrique",
	edad: 45
});

console.log(villamaravilla);
console.log(pepe);
console.log(lenrique);
villamaravilla.jugarPartido();
lenrique.dirigirPartido();