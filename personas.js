var Persona = function(datos){
	this.nombre = "";
	this.direccion = "";
	this.correoElectronico = "";

	this.crear = function(){
		console.log("Persona nueva creada.");
	};

	this.borrar = function(){
		console.log("Persona borrada.");
	};

	this.enviarMensaje = function(){
		console.log("Enviamos el mensaje.");
	};

	this.persona = function(datos){
		this.nombre = datos.nombre;
		this.direccion = datos.direccion;
		this.correoElectronico = datos.correoE;
	};

	this.persona(datos);
};

var Cliente = function(datos){
	this.numeroCliente = 0;
	this.fechaAlta = "";

	this.verFechaAlta = function(){
		console.log("FECHA DE ALTA " + this.fechaAlta);
	};

	this.cliente = function(datos){
		Persona.call(this, datos);
		this.numeroCliente = datos.numeroCliente;
		this.fechaAlta = datos.fechaAlta;
	};

	this.cliente(datos);
};

var Usuario = function(datos){
	this.codigoUsuario = 0;

	this.autorizar = function(){
		console.log("Usuario con el código " + this.codigoUsuario + " autorizado.");
	};

	this.crear = function(){
		console.log("Usuario con el código " + this.codigoUsuario + " creado.");
	};

	this.usuario = function(datos){
		Persona.call(this, datos);
		this.codigoUsuario = datos.codigo;
	}

	this.usuario(datos);
};

var pepe = new Cliente({
	nombre: "Pepe Basurilla",
	direccion: "13 Rue del Percebe",
	correoE: "somethingsomethingcomplete",
	numeroCliente: 1,
	fechaAlta: "2 de febrero de 2018"
});

pepe.verFechaAlta();

var yon = new Usuario({
	nombre: "Yon Maclein",
	direccion: "Barrio la Ercilla 72",
	correoE: "nenenananunu",
	codigo: 672
});

console.log(pepe);
console.log(yon);

yon.crear();